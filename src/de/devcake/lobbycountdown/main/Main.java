package de.devcake.lobbycountdown.main;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import de.devcake.lobbycountdown.listener.ListenerJoin;

public class Main extends JavaPlugin {
	
	private static Main instance;
	
	public static String prefix = "§eLobbyCountdown >> ";
	
	@Override
	public void onEnable() {
		System.out.println("LobbyCountdown ist Aktiv!");
		System.out.println("Autor: " + getDescription().getAuthors());
		System.out.println("Version: " + getDescription().getVersion());
		
		Bukkit.getPluginManager().registerEvents(new ListenerJoin(), this);
		
		instance = this;
		
	}
	
	@Override
	public void onDisable() {
		System.out.println("LobbyCountdown ist nicht mehr Aktiv!");
	}
	
	public static Main getInstance() {
		return instance;
	}
	
}
