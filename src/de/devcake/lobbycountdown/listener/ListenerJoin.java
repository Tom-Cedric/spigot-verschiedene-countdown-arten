package de.devcake.lobbycountdown.listener;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitTask;

import de.devcake.lobbycountdown.main.Main;

public class ListenerJoin implements Listener {
	
	/*
	 * @Author: DevCake
	 * 
	 * 
	 */
	
	private BukkitTask bukkittask;
	private Integer cd = 60; //cd = countdown
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		
		Player player = (Player) event.getPlayer();
		
		bukkittask = Bukkit.getScheduler().runTaskTimer(Main.getInstance(), new Runnable() {
			
			@Override
			public void run() {
				
				/*
				 * Methode zum Anzeigen des Countdowns durch die XP-Leiste und der Level Anzeige.
				 */
				for (Player allPlayers : Bukkit.getOnlinePlayers()) {
					
					//Setzen des Countdowns als Level.
					allPlayers.setLevel(cd);
					
					//Setzen der XP-Leiste mit der Countdown Zeit.
					allPlayers.setExp((float)  cd / 60);
					
				}
				//------------------------------------------------------------------------------------
				
				/*
				 * Methode zum Anzeigen des Countdowns durch den Chat.
				 */
				//Damit wird jede Sekunde eine Nachricht gesendet. Wo die Countdown Zeit zu sehen ist.
				player.sendMessage(Main.prefix + "�eDer Countdown betr�gt noch: �7" + cd + "�es");
				//------------------------------------------------------------------------------------
				
				/*
				 * Methode zur Einsetzung von Sound Effekten bei Countdowns.
				 */
				for (Player allPlayers : Bukkit.getOnlinePlayers()) {
					
					//Damit wird der Sound jede Sekunde abgespielt.
					allPlayers.playSound(player.getLocation(), Sound.BLOCK_NOTE_PLING, 2.0F, 2.0F);
					
				}
				//-------------------------------------------------------------------------------------
				
				//Hier wird gepr�ft was passiert, wenn der Countdown auf 0 ist.
				if (cd == 0) {
					
					//Countdown Zeit wird neu gesetzt.
					cd = 60;
					
					//Scheduler wird beendet.
					bukkittask.cancel();
					
				}
				
				//Der Countdown zieht immer eins ab.
				cd--;
				
			}
			
		}, 0l, 20l);
		
	}

}
